const app = Vue.createApp({
    data() {
      return {
        paineis: [
          {
            name: "Atlas do Capital Humano",
            url: "../atlas_capital_humano/",
            image: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/images/dashboard_card_preview/banner_atlas_capital_humano-900x450.svg",
            description: "Panorama do Capital Humano no município de Fortaleza"
          },
          {
            name: "Observatório da Juventude",
            url: "https://observatoriodejuventude.fortaleza.ce.gov.br/index.php/estatisticas-e-dados/paineis-de-indicadores",
            image: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/images/dashboard_card_preview/banner_observatorio_de_juventude-900x450.svg",
            description: "Informações estratégicas sobre a juventude de Fortaleza"
          },
          {
            name: "Censo da População de Rua",
            url: "https://app.powerbi.com/view?r=eyJrIjoiZWVhZGMxNjUtMGMxYS00ZjVjLWFhNzQtZTJkY2VjNzliOGE0IiwidCI6ImE0ZTA2MDVjLWUzOTUtNDZlYS1iMmE4LThlNjE1NGM5MGUwNyJ9",
            image: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/images/dashboard_card_preview/banner_censo_populacao_de_rua-900x450.svg",
            description: "Norteando as políticas públicas para a população mais vulnerável"
          },
	        {
            name: "Fortaleza em Mapas",
            url: "https://mapas.fortaleza.ce.gov.br/ ",
            image: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/images/dashboard_card_preview/banner_fortaleza_em_mapas-900x450.svg",
            description: "Informações geográficas sobre o município de Fortaleza "
	        },
	        {
		        name: "Plataforma VIDA",
		        url: "https://vida.centralamc.com.br/",
		        image: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/images/dashboard_card_preview/banner_plataforma_vida-900x450.svg",
		        description: "Informação para Gestão da Segurança no Trânsito"
	        }
	      ]
      }
    }
  }).mount('#app')
