<!--
.. title: Como criar um dicionário de dados
.. author: Observatório de Fortaleza
.. slug: como-criar-um-dicionario-de-dados
.. date: 2021-09-28 14:33:17 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

A exploração de dados se inicia com uma boa organização. Um *dicionário de dados* é uma espécie de glossário: uma lista de termos e métricas com suas respectivas descrições e definições. O dicionário de dados é fundamental para tornar uma pesquisa reprodutível, pois possibilita que outras pessoas entendam o contexto da coleta e armazenamento do conjunto de dados. O seu propósito principal é explicar quais as variáveis do conjunto de dados, seus valores e o que significam.
<!-- TEASER_END -->

Considere por exemplo o seguinte conjunto de dados:
```{csv}
nome_da_variavel, definicao, tipo_variavel, valores_permitidos, exemplo
ID_PART, Identificador único atribuído a cada participante sequencialmente, integer, 001-999
NM_PART, Nome do participante, varchar,,
DT_NASC, Data de nascimento do participante, date, 01-31/01-12/1900-2021
SEXO, Sexo do participante, integer, 1=FEMININO; 2=MASCULINO
```

## Nome da Variável

A primeira coluna deve conter o nome da variável exatamente como ele aparece na tabela/arquivo/planilha.

Alguns prefixos podem ajudar a identificar a natureza da variável de antemão:

- `NM_` :: Nomes
- `TP_` :: Tipo
- `DT_` :: Data
- `CD_` ou `CO_` :: Código
- `QT_` :: Quantidade
- `AN_` :: Ano
- `ME_` :: Mês
- `ID_` :: Identificador
- `IN_` :: Presença ou ausência de uma característica

## Definição

Definição da variável. Ela deve refletir a forma que você a utiliza, com a intenção de que outros possam entender o seu uso.

Existem várias formas de se definir a variável mas, quando possível, formulá-la na forma de <a href="https://en.wikipedia.org/wiki/Genus%E2%80%93differentia_definition" target="_blank" title="Diferenciação de Classe (abre em uma nova aba)" aria-label="Diferenciação de Classe (abre em uma nova aba)">diferenciação de classe</a>: "A é um B que C.". Por exemplo, "Um A) triângulo é uma B) figura plana que C) possui três lados. 

Evite usar definições circulares como "Uma bola de futebol é uma bola usada no futebol."

## Tipo da Variável

Contém o tipo e/ou unidade de medida da variável. Em se tratando de variáveis tabulares, seguir o padrão de <a href="https://www.postgresql.org/docs/current/datatype.html" target="_blank" title="Tipos de Dados do PostgreSQL (abre em uma nova aba)" aria-label="Tipos de Dados do PostgreSQL (abre em uma nova aba)">tipos do PostgreSQL</a>. Por exemplo:

- `integer`: Variáveis numéricas inteiras (escolha mais comum no postgres)
- `varchar`, `text`: para dados textuais. No caso de haver um tamanho máximo `N` para a coluna, especificar como `varchar(N)`.
- `real`: Variáveis numéricas de ponto flutuante
- `date`: Variáveis que especificam datas.
  
## Valores permitidos (opcional)

Faixa de valores permitidos para a variável. Ajuda a identificar observações (linhas) incorretas, e/ou verificar se a leitura dos dados foi efetuada corretamente.

Para variáveis do tipo `date`, olhar a <a href="https://www.ietf.org/rfc/rfc3339.txt" target="_blank" title="Especificação de datas na Internet (abre em uma nova aba)" aria-label="Especificação de datas na Internet (abre em uma nova aba)">especificação</a> para formatação.

## Referências

A principal referência é a *Iniciativa para Documentação de Dados* (<a href="https://ddialliance.org/" target="_blank" title="Data Documentation Initiative (abre em uma nova aba)" aria-label="Data Documentation Initiative (abre em uma nova aba)">Data Documentation Initiative</a> - DDI), um padrão internacional descrevendo dados produzidos por questionários e outros métodos observacionais em ciências sociais, econômicas e de saúde. 

Alguns exemplos de dicionários de dados podem ser encontrados na página do <a href="https://www.usgs.gov/data-management/data-dictionaries" target="_blank" title="Serviço Geológico dos Estados Unidos (abre em uma nova aba)" aria-label="Serviço Geológico dos Estados Unidos (abre em uma nova aba)">Serviço Geológico dos Estados Unidos</a>.
