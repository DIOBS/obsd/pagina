<!--
.. title:   
.. slug: index
.. date: 2022-06-10 15:37:11 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

The *Data Observatory* is a division of the <a href="https://observatoriodefortaleza.fortaleza.ce.gov.br/" target="_blank" title="Observatório de Fortaleza (abre em uma nova aba)" aria-label="Fortaleza Observatory (opens a new tab)">Fortaleza Observatory</a> focused on the management, training and promotion of Data Science at the Fortaleza Planning Institute (IPLANFOR).

The Observatory assists in the process of publicizing the data and analyzes carried out by the Planning Institute, making the City Hall's decision-making process accessible and understandable to public managers and citizens.

With this we hope to document part of the history of the city and bring the public management closer to the population of Fortaleza.

