<!--
.. title:   
.. slug: index
.. date: 2022-06-10 15:37:11 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

O *Observatório em Dados* é uma divisão do <a href="https://observatoriodefortaleza.fortaleza.ce.gov.br/" target="_blank" title="Observatório de Fortaleza (abre em uma nova aba)" aria-label="Observatório de Fortaleza (abre em uma nova aba)">Observatório de Fortaleza</a> voltada para o gerenciamento, a formação e fomento em Ciência de Dados no Instituto de Planejamento de Fortaleza (IPLANFOR).

O Observatório auxilia no processo de publicização dos dados e análises feitas pelo Instituto de Planejamento, tornando o processo de tomada de decisões da Prefeitura acessíveis e compreensíveis aos gestores públicos e aos cidadãos. 

Com isso esperamos documentar parte da história da cidade e aproximar a gestão municipal da população Fortalezense.

